﻿using System.Collections.Generic;

namespace KeyCounter.Stats
{
	public interface IStatsPersister
	{
		void Persist(IReadOnlyList<KeyStat> a_stats);
	}
}