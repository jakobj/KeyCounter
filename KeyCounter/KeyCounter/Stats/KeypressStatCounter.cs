﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

namespace KeyCounter.Stats
{
	public class KeypressStatCounter
	{
		private readonly IReadOnlyList<KeyStat> m_initStats;

		private readonly List<KeyStat> m_keysStats = new List<KeyStat>();

		private readonly short[] m_keyCodes = new short[1025];

		public IReadOnlyList<KeyStat> KeysStats => m_keysStats;

		public KeypressStatCounter(IReadOnlyList<KeyStat> a_initStats = null)
		{
			m_initStats = a_initStats;
			GenerateKeysList();
		}

		private void GenerateKeysList()
		{
			short index = 0;
			foreach (Keys key in Enum.GetValues(typeof(Keys)))
			{
				if (key == Keys.KeyCode || key == Keys.Modifiers)
					continue;

				//HACK: Enter and return has same key code
				if (m_keysStats.Exists(ks => ks.Key == key))
					continue;

				KeyStat initStatForKey = m_initStats?.FirstOrDefault(s => s.Key == key);
				long count = initStatForKey == null ? 0 : initStatForKey.Count;

				m_keysStats.Add(new KeyStat(key, count));

				m_keyCodes[Index(key)] = index++;
			}
		}

		public void AddKey(Keys a_key)
		{
			m_keysStats[GetIndex(a_key)].Increment();
		}

		private int GetIndex(Keys a_key)
		{
			return m_keyCodes[Index(a_key)];
		}

		private static int Index(Keys a_key)
		{
			int modifier = (int) a_key & (int) Keys.Modifiers;
			int keyCode = (int) a_key & (int) Keys.KeyCode;
			int index = (modifier >> 8) | keyCode;
			return index;
		}
	}

	[DebuggerDisplay("{Key}: {Count}")]
	public class KeyStat
	{
		public Keys Key { get; }

		public long Count => m_count;
		private long m_count;

		public KeyStat(Keys a_key)
		{
			Key = a_key;
		}

		public KeyStat(Keys a_key, long a_count)
			: this(a_key)
		{
			m_count = a_count;
		}

		public void Increment()
		{
			m_count++;
		}
	}
}