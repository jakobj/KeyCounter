﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace KeyCounter.Stats.Impl
{
	public class CsvKeyStatsPersister : IStatsPersister, IStatsLoader
	{
		private const String Separator = ";";

		private static readonly String[] SeparatorList = { Separator };

		private readonly String m_directory;

		private static readonly IReadOnlyList<KeyStat> Empty = new KeyStat[0];

		public CsvKeyStatsPersister(String a_directory)
		{
			m_directory = a_directory;
		}

		public void Persist(IReadOnlyList<KeyStat> a_stats)
		{
			String contents = String.Join(Environment.NewLine, a_stats.Select(ks => ks.Key + Separator + ks.Count));
			contents = "Key;Count" + Environment.NewLine + contents;

			File.WriteAllText(GetFilePath(), contents);
		}

		public IReadOnlyList<KeyStat> Load()
		{
			String filePath = GetFilePath();

			if (!File.Exists(filePath))
				return Empty;

			String[] lines = File.ReadAllLines(filePath);

			List<KeyStat> readStats = new List<KeyStat>();
			for (int i = 1; i < lines.Length; i++)
			{
				String[] keyAndCount = lines[i].Split(SeparatorList, StringSplitOptions.None);
				Keys keys = (Keys)Enum.Parse(typeof(Keys), keyAndCount[0]);
				readStats.Add(new KeyStat(keys, int.Parse(keyAndCount[1])));
			}

			return readStats;
		}

		private String GetFilePath()
		{
			String fileName = "keystats_" + DateTime.Now.ToString("yyyy-MM-dd") + ".csv";
			return Path.Combine(m_directory, fileName);
		}
	}
}