﻿using System.Collections.Generic;

namespace KeyCounter.Stats
{
	public interface IStatsLoader
	{
		IReadOnlyList<KeyStat> Load();
	}
}