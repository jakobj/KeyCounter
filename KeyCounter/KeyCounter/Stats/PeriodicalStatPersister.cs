﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace KeyCounter.Stats
{
	public class PeriodicalStatPersister : IDisposable
	{
		private Timer m_periodicalSaveTimer;

		private readonly KeypressStatCounter m_counter;

		private readonly IStatsPersister m_statsPersister;

		public PeriodicalStatPersister(KeypressStatCounter a_counter, IStatsPersister a_statsPersister)
		{
			m_counter = a_counter;
			m_statsPersister = a_statsPersister;
		}

		public void StartMonitor()
		{
			m_periodicalSaveTimer = new Timer(SaveStats, null, TimeSpan.FromSeconds(30), TimeSpan.FromMinutes(1));
		}

		private void SaveStats(object a_state)
		{
			IReadOnlyList<KeyStat> list = m_counter.KeysStats;
			List<KeyStat> onlyPressedKeys = list.Where(ks => ks.Count > 0).ToList();
			m_statsPersister.Persist(onlyPressedKeys);
		}

		public void Dispose()
		{
			m_periodicalSaveTimer.Dispose();
		}
	}
}