﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace KeyCounter.Interop
{
	public sealed class InterceptKeys
	{
		private const int WH_KEYBOARD_LL = 13;

		private const int WM_KEYDOWN = 0x0100;

		private const int WM_KEYUP = 0x0101;

		private readonly LowLevelKeyboardProc _proc;

		private IntPtr _hookID = IntPtr.Zero;

		private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);

		public event Action<Keys> KeyDown;

		public InterceptKeys()
		{
			_proc = HookCallback;
		}

		public void StartMonitoring()
		{
			_hookID = SetHook(_proc);
		}

		public void StopMonitoring()
		{
			UnhookWindowsHookEx(_hookID);
			_hookID = IntPtr.Zero;
		}

		private void OnKeyDown(Keys keys)
		{
			KeyDown?.Invoke(keys);
		}

		private IntPtr SetHook(LowLevelKeyboardProc proc)
		{
			using (Process curProcess = Process.GetCurrentProcess())
			using (ProcessModule curModule = curProcess.MainModule)
			{
				return SetWindowsHookEx(WH_KEYBOARD_LL, proc, GetModuleHandle(curModule.ModuleName), 0);
			}
		}

		private IntPtr HookCallback(int code, IntPtr wParam, IntPtr lParam)
		{
			try
			{
				if (code >= 0 && wParam == (IntPtr)WM_KEYDOWN)
				{
					int vkCode = Marshal.ReadInt32(lParam);
					OnKeyDown((Keys)vkCode);
				}
			}
			catch (Exception)
			{
			}

			return CallNextHookEx(_hookID, code, wParam, lParam);
		}

		public static bool GetShiftPressed()
		{
			int state = GetKeyState(Keys.ShiftKey);
			return state > 1 || state < -1;
		}

		public static bool GetCtrlPressed()
		{
			int state = GetKeyState(Keys.ControlKey);
			return state > 1 || state < -1;
		}

		public static bool GetAltPressed()
		{
			int state = GetKeyState(Keys.Menu);
			return state > 1 || state < -1;
		}

		[DllImport("user32.dll")]
		static public extern short GetKeyState(Keys nVirtKey);

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern IntPtr SetWindowsHookEx(int idHook, LowLevelKeyboardProc lpfn, IntPtr hMod, uint dwThreadId);

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool UnhookWindowsHookEx(IntPtr hhk);

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);

		[DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern IntPtr GetModuleHandle(string lpModuleName);
	}
}