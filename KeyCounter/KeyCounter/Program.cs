﻿using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using KeyCounter.Interop;
using KeyCounter.Stats;
using KeyCounter.Stats.Impl;
using KeyCounter.ViewModels;

namespace KeyCounter
{
	public class Program
	{
		private static KeypressStatCounter m_counter;

		[STAThread]
		public static void Main()
		{
			AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;

			String localAppDataPath = InitDataDirectory();
			CsvKeyStatsPersister statsPersister = new CsvKeyStatsPersister(localAppDataPath);

			m_counter = new KeypressStatCounter(statsPersister.Load());

			App app = new App();

			InterceptKeys interceptKeys = new InterceptKeys();
			interceptKeys.KeyDown += GlobalKeyDown;
			interceptKeys.StartMonitoring();

			using (PeriodicalStatPersister statPersister = new PeriodicalStatPersister(m_counter, statsPersister))
			{
				statPersister.StartMonitor();

				MainWindow mainWindow = new MainWindow
				{
					ViewModel = new MainViewModel(m_counter)
				};
				app.Run(mainWindow);

				interceptKeys.StopMonitoring();
			}
		}

		private static void CurrentDomainOnUnhandledException(object a_sender, UnhandledExceptionEventArgs a_exc)
		{
			String codeBase = Assembly.GetExecutingAssembly().CodeBase;
			File.WriteAllText(Path.Combine(Path.GetDirectoryName(codeBase), "exception" + DateTime.Now.ToString("yyyyMMdd_HHmmss")), a_exc.ToString());
		}

		private static string InitDataDirectory()
		{
			String folderPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
			String localAppDataPath = Path.Combine(folderPath, "KeyCounter");

			if (!Directory.Exists(localAppDataPath))
				Directory.CreateDirectory(localAppDataPath);
			return localAppDataPath;
		}

		private static void GlobalKeyDown(Keys a_keys)
		{
			m_counter.AddKey(a_keys);
		}
	}
}