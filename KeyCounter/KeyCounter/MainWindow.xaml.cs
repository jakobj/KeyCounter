﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Resources;
using System.Windows.Threading;
using KeyCounter.ViewModels;
using Application = System.Windows.Application;

namespace KeyCounter
{
	public partial class MainWindow
	{
		private NotifyIcon m_notifyIcon;

		public MainViewModel ViewModel
		{
			get { return DataContext as MainViewModel; }
			set { DataContext = value; }
		}

		public MainWindow()
		{
			InitializeComponent();
			InitializeNotyfingIcon();
		}

		private void InitializeNotyfingIcon()
		{
			Uri uri = new Uri("keyboard.ico", UriKind.Relative);
			StreamResourceInfo info = Application.GetResourceStream(uri);

			m_notifyIcon = new NotifyIcon();
			m_notifyIcon.DoubleClick += m_notifyIcon_DoubleClick;
			m_notifyIcon.Icon = new Icon(info.Stream);
			m_notifyIcon.Visible = true;

			ContextMenuStrip contextMenu = new ContextMenuStrip();

			ToolStripItem close = new ToolStripMenuItem("Zamknij", null, CloseApp);
			contextMenu.Items.Add(close);

			m_notifyIcon.ContextMenuStrip = contextMenu;
		}

		private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			if (WindowState == WindowState.Minimized)
				Visibility = Visibility.Hidden;
		}

		private void Window_StateChanged(object sender, EventArgs e)
		{
			if (WindowState == WindowState.Minimized)
				Visibility = Visibility.Hidden;
			else if (WindowState == WindowState.Normal)
				Visibility = Visibility.Visible;
		}

		private void CloseApp(Object sender, EventArgs e)
		{
			Close();
		}

		void m_notifyIcon_DoubleClick(object sender, EventArgs e)
		{
			if (WindowState == WindowState.Minimized)
			{
				Visibility = Visibility.Visible;
				ShowInTaskbar = true;
				Application.Current.Dispatcher.BeginInvoke(new Action(() =>
				{
					WindowState = WindowState.Normal;
					Activate();
				}), DispatcherPriority.Background);
			}
			else if (WindowState == WindowState.Normal)
			{
				WindowState = WindowState.Minimized;
				ShowInTaskbar = false;
			}
		}

		private void WindowClosing(object a_sender, CancelEventArgs a_e)
		{
			m_notifyIcon.Dispose();
		}
	}
}
