﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using KeyCounter.Stats;

namespace KeyCounter.ViewModels
{
	public class MainViewModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		public ObservableCollection<PressedKeyViewModel> PressedKess
		{
			get { return m_pressedKess; }
			set
			{
				m_pressedKess = value;
				OnPropertyChanged();
			}
		}
		private ObservableCollection<PressedKeyViewModel> m_pressedKess = new ObservableCollection<PressedKeyViewModel>();

		private KeypressStatCounter m_counter;

		public MainViewModel(KeypressStatCounter a_counter)
		{
			m_counter = a_counter;
		}

		protected virtual void OnPropertyChanged([CallerMemberName] String a_propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(a_propertyName));
		}
	}

	public class PressedKeyViewModel : INotifyPropertyChanged
	{
		public Keys Key { get; set; }

		public int Count { get; set; }

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged([CallerMemberName] string a_propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(a_propertyName));
		}
	}
}